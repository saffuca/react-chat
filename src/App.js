import 'bootstrap/dist/css/bootstrap.min.css';
import { Route } from 'react-router-dom';
import styled from 'styled-components';
import Menu from './components/menu/Menu.js';

const AppWrapper = styled.div`
display: flex;
min-height: 100vh;
background: ${props => props.background};
`;

function App() {
  return (
    <div className="App">
      <AppWrapper background="#fff">
        <Menu/>
      </AppWrapper>
    </div>

  );
}

export default App;
