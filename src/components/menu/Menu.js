import React, { useState } from 'react';
import styled from 'styled-components';
import {
    HomeOutline,
    PersonOutline,
    ChatbubblesOutline,
    SettingsOutline,
    HelpOutline,
    LockClosedOutline,
    LogOutOutline
} from 'react-ionicons';
import { Link } from 'react-router-dom';

const Navigation = styled.div`
    position: relative;
    height: 100vh;
    width: 70px;
    background: #2b343b;
    box-shadow: 10px 0 0 #4187f6;
    border-left: 10px solid #2b343b;
    overflow-x: hidden;
    transition: width 0.5s;
  
  &:hover {
    width: 300px;
  }
  
  & ul {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    padding-top: 40px;
  }
  
  & ul li {
    position: relative;
    list-style: none;
    width: 100%;
    border-top-left-radius: 20px;
    border-bottom-left-radius: 20px;
  }
  
  & ul li.active {
    background: #4187f6;
  }
  
  & ul li a {
    position: relative;
    display: block;
    width: 100%;
    display: flex;
    text-decoration: none;
    color: #fff;
  }
  
  & ul li.active a::before {
    content: '';
    position: absolute;
    top: -30px;
    right: 0;
    width: 30px;
    height: 30px;
    background: #2b343b;
    border-radius: 50%;
    box-shadow: 15px 15px 0 #4187f6;
  }
  
  & ul li.active a::after {
    content: '';
    position: absolute;
    bottom: -30px;
    right: 0;
    width: 30px;
    height: 30px;
    background: #2b343b;
    border-radius: 50%;
    box-shadow: 15px -15px 0 #4187f6;
  }
  
  & ul li a .icon {
    position: relative;
    display: block;
    min-width: 60px;
    height: 60px;
    line-height: 60px;
    text-align: center;
    z-index: 1;
  }
  
 
  & ul li a .title {
    position: relative;
    display: block;
    padding-left: 10px;
    height: 60px;
    line-height: 60px;
    white-space: nowrap;
    z-index: 1;
  }
`;

function Menu() {
    const [activeMenuItem, setActiveMenuItem] = useState("0");
    const iconColor = '#ffffff';
    const iconWidth = '1.5em';
    const iconHeight = '1.5em';

    function clickHandle(event) {
        setActiveMenuItem(event.target.closest('li').dataset.count);
    }

    return (
        <Navigation className="navigation">
            <ul>
                <li onClick={clickHandle} data-count="0" className={activeMenuItem === "0" ? "list active" : "list"}>
                    <Link to="#">
                        <span className="icon">
                            <HomeOutline
                                color={iconColor}
                                height={iconHeight}
                                width={iconWidth}
                            />
                        </span>
                        <span className="title">Home</span>
                    </Link>
                </li>
                <li onClick={clickHandle} data-count="1" className={activeMenuItem === "1" ? "list active" : "list"}>
                    <Link to="#">
                        <span className="icon">
                            <PersonOutline
                                color={iconColor}
                                height={iconHeight}
                                width={iconWidth}
                            />
                        </span>
                        <span className="title">Profile</span>
                    </Link>
                </li>
                <li onClick={clickHandle} data-count="2" className={activeMenuItem === "2" ? "list active" : "list"}>
                    <Link to="#">
                        <span className="icon">
                            <ChatbubblesOutline
                                color={iconColor}
                                height={iconHeight}
                                width={iconWidth}
                            />
                        </span>
                        <span className="title">Messages</span>
                    </Link>
                </li>
                <li onClick={clickHandle} data-count="3" className={activeMenuItem === "3" ? "list active" : "list"}>
                    <Link to="#">
                        <span className="icon">
                            <SettingsOutline
                                color={iconColor}
                                height={iconHeight}
                                width={iconWidth}
                            />
                        </span>
                        <span className="title">Setting</span>
                    </Link>
                </li>
                <li onClick={clickHandle} data-count="4" className={activeMenuItem === "4" ? "list active" : "list"}>
                    <Link to="#">
                        <span className="icon">
                            <HelpOutline
                                color={iconColor}
                                height={iconHeight}
                                width={iconWidth}
                            />
                        </span>
                        <span className="title">Help</span>
                    </Link>
                </li>
                <li onClick={clickHandle} data-count="5" className={activeMenuItem === "5" ? "list active" : "list"}>
                    <Link to="#">
                        <span className="icon">
                            <LockClosedOutline
                                color={iconColor}
                                height={iconHeight}
                                width={iconWidth}
                            />
                        </span>
                        <span className="title">Password</span>
                    </Link>
                </li>
                <li onClick={clickHandle} data-count="6" className={activeMenuItem === "6" ? "list active" : "list"}>
                    <Link to="#">
                        <span className="icon">
                            <LogOutOutline
                                color={iconColor}
                                height={iconHeight}
                                width={iconWidth}
                            />
                        </span>
                        <span className="title">Sign out</span>
                    </Link>
                </li>
            </ul>
        </Navigation>
    );
}

export default Menu;